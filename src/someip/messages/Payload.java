/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.messages;

import java.util.Arrays;

public class Payload {

	// See [TR_SOMEIP_00139]
	final static int DEFAULT_MTU_SIZE = 1500;
	private int payloadSize = DEFAULT_MTU_SIZE;
	private byte[] data = new byte[payloadSize];
	private int offset = 0;

	// Trim the byte array by copying it
	public byte[] toBytes() {
		byte[] trim = new byte[offset];
		System.arraycopy(data, 0, trim, 0, offset);

		return trim;
	}

	public int fromBytes(byte[] ndata) {
		data = ndata;
		return getOffset();
	}

	protected int getOffset() {
		return offset;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] ndata) {
		data = ndata;
	}

	public void DuplicateArraySize() {
		payloadSize *= 2;
		byte[] tmp = new byte[payloadSize];
		System.arraycopy(data, 0, tmp, 0, data.length);
		data = tmp;
	}

	public void setOffset(int offset) {
		if(offset + 1 > data.length)
			DuplicateArraySize();

		this.offset = offset;
	}

	public String toString() {
		return "Raw Data: " + Arrays.toString(data) + "\n";
	}

	public void serializeByte(byte v) {
		if(offset + 1 > data.length)
			DuplicateArraySize();

		data[offset++] = v;
	}

	public void serializeShort(short v) {
		serializeByte((byte) ((v & 0xFF00) >> 8));
		serializeByte((byte) (v & 0x00FF));
	}

	public void serializeInt(int v) {
		serializeByte((byte) ((v & 0xFF000000) >> 24));
		serializeByte((byte) ((v & 0x00FF0000) >> 16));
		serializeByte((byte) ((v & 0x0000FF00) >> 8));
		serializeByte((byte) (v & 0x000000FF));
	}

	public void serializeLong(long v) {
		serializeInt((int)(v & 0x00000000FFFFFFFF));
		serializeInt((int)(v >> 32));
	}

	public void serializeBytes(byte[] ndata) {

		while(data.length < offset+ndata.length)
			DuplicateArraySize();

		if(ndata != null && ndata.length != 0) {
			System.arraycopy(ndata, 0, data, offset, ndata.length);
			offset += ndata.length;
		}
	}

	public void serializeTLV(byte[] ndata) {
		serializeInt(ndata.length);
		serializeBytes(ndata);
	}

	public byte deserializeByte() {
		byte v = data[offset++];
		return v;
	}

	public short deserializeShort() {
		byte v1 = deserializeByte();
		byte v2 = deserializeByte();
		return (short) (((0xFF & v1) << 8) | (0xFF & v2));
	}

	public int deserializeInt() {
		byte v1 = deserializeByte();
		byte v2 = deserializeByte();
		byte v3 = deserializeByte();
		byte v4 = deserializeByte();
		return (int) ((0xFF & v1) << 24) | ((0xFF & v2) << 16) | ((0xFF & v3) << 8) | (0xFF & v4);
	}

	public long deserializeLong() {
		long a  = deserializeInt();
		long b  = deserializeInt();

		return a | (b << 32);
	}

	public byte[] deserializeBytes(int size) {
		byte[] b = new byte[size];
		System.arraycopy(data, offset, b, 0, size);

		offset += size;

		return b;
	}

	public byte[] deserializeTLV() {
		int length = deserializeInt();
		byte[] ndata = deserializeBytes(length);

		return ndata;
	}

	public void deserializeSkip(int size) {
		offset += size;
	}
}
