/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.messages;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

// See [TR_SOMEIP_00309]
public class SDIPv4EndpointOption extends Payload {

	public SDIPv4EndpointOption() {
		// See [TR_SOMEIP_00309]
		setLength(0x0009);
		setType(0x04);
	}

	public byte[] toBytes() {
		serializeShort(Length);
		serializeByte(Type);
		serializeByte((byte) 0x00);
		serializeInt(IPv4Address);
		serializeByte((byte) 0x00);
		serializeByte(TransportProtocol);
		serializeShort(TransportProtocolPortNumber);

		// Trim
		byte[] trim = new byte[getOffset()];
		System.arraycopy(getData(), 0, trim, 0, getOffset());

		return trim;
	}

	public int fromBytes(byte[] data) {
		setData(data);

		Length = deserializeShort();
		Type = deserializeByte();
		deserializeByte();
		IPv4Address = deserializeInt();
		deserializeByte();
		TransportProtocol = deserializeByte();
		TransportProtocolPortNumber = deserializeShort();

		return getOffset();
	}

	private String intToIP(int ip) {
		return  String.format("%d.%d.%d.%d",
			 (ip >> 24 & 0xff),
			 (ip >> 16 & 0xff),
			 (ip >> 8 & 0xff),
			 (ip & 0xff));
	}

	public String toString() {
		return "Service Option " + "Type: " + (int) (0xFF & Type) + ", IPv4Address: "
				+ intToIP(IPv4Address) + ""
				+ ", TransportProtocolPortNumber: " + TransportProtocolPortNumber
				+ ", TransportProtocol: " + TransportProtocol;
	}

	private void setType(int Type) {
		this.Type = (byte) Type;
	}

	private void setLength(int Length) {
		this.Length = (short) Length;
	}

	public int getIPv4Address() {
		return IPv4Address;
	}

	public String getIPv4AddressString() {
		try {
			byte[] bytes = BigInteger.valueOf(IPv4Address).toByteArray();
			InetAddress address = InetAddress.getByAddress(bytes);
			return address.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void setIPv4Address(String IPv4Address) {
		try {
			InetAddress addr = InetAddress.getByName(IPv4Address);
			int IPv4AddressInt = ByteBuffer.wrap(addr.getAddress()).getInt();
			this.IPv4Address = IPv4AddressInt;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public void setIPv4Address(int IPv4Address) {
		this.IPv4Address = IPv4Address;
	}

	public byte getTransportProtocol() {
		return TransportProtocol;
	}

	public void setTransportProtocol(byte transportProtocol) {
		this.TransportProtocol = transportProtocol;
	}

	public short getTransportProtocolPortNumber() {
		return TransportProtocolPortNumber;
	}

	public void setTransportProtocolPortNumber(short transportProtocolPortNumber) {
		this.TransportProtocolPortNumber = transportProtocolPortNumber;
	}

	final static int TCP = 0x06;
	final static int UDP = 0x11;

	// Shall be set to 0x0009.
	private short Length;

	// Shall be set to 0x24.
	private byte Type;

	// Shall transport the unicast IP-Address of SOME/IP as
	// four Bytes.
	private int IPv4Address;

	// Shall be set to the transport layer protocol
	// (ISO/OSI layer 4) based on the IANA/IETF types (0x06: TCP, 0x11: UDP).
	private byte TransportProtocol;

	// Shall be set to the transportlayer port of SOME/IP-SD (e.g. 30490).
	private short TransportProtocolPortNumber;
}
