/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.messages;

public class SDEventGroupEntry extends Payload {

	public byte[] toBytes() {
		return new byte[]{000000000000000000000};
	}

	public int fromBytes(byte[] data) {
		setData(data);

		type = deserializeByte();
		firstOptionRun = deserializeByte();
		secondOptionRun = deserializeByte();

		int options = deserializeByte();
		options1 = (byte) (options >> 4);
		options2 = (byte) (options & 0x0F);

		serviceId = deserializeShort();
		instanceId = deserializeShort();

		int tmp1 = deserializeInt();
		major = (byte) (tmp1 >> 24);
		TTL = tmp1 & 0x0FFF;

		int tmp2 = deserializeShort();
		counter = (byte) (tmp2 & 0x000F);

		eventgroupId = deserializeShort();

		return getOffset();
	}

	public String toString() {
		return "Service Entry " + "Type: " + (int) (0xFF & type) + ", ServiceId: "
				+ serviceId + ", InstanceId: " + instanceId;
	}

	// ReturnCodes [TR_SOMEIP_00270]
	final static int SUBSCRIBE = 0x06;
	final static int SUBSCRIBE_ACK = 0x07;

	//Type Field [uint8]: encodes Subscribe (0x06), and SubscribeAck (0x07).
	private byte type = 0;

	//Index First Option Run [uint8]: Index of this runs first option in the option array.
	private byte firstOptionRun;

	//Index Second Option Run [uint8]: Index of this runs second option in the option array.
	private byte secondOptionRun;

	//Number of Options 1 [uint4]: Describes the number of options the first option run	uses.
	private byte options1;

	// Number of Options 2 [uint4]: Describes the number of options the second option run uses.
	private byte options2;

	// Service-ID [uint16]: Describes the Service ID of the Service or Service Instance	this entry is concerned with.
	private short serviceId = 0;

	// Instance ID [uint16]: Describes the Service Instance ID of the Service Instance
	// this entry is concerned with or is set to 0xFFFF if all service instances of a service
	// are meant.
	private short instanceId = 0;

	// Major Version [uint8]: Encodes the major version of the service instance this
	// eventgroup is part of.
	private byte major = 0;

	//TTL [uint24]: Descibes the lifetime of the entry in seconds.
	private int TTL = 0;

	// Reserved [uint12]: Shall be set to 0x000, if not specified otherwise (see
	// [TR_SOMEIP_00391] and [TR_SOMEIP_00394]).
	private short reserved;

	// Counter [uint4]: Is used to differentiate identical Subscribe Eventgroups. Set to
	// 0x0 if not used.
	private byte counter = 0;

	// Eventgroup ID [uint16]: Transports the ID of an Eventgroup.
	private short eventgroupId = 0;
}
