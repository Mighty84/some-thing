/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

// See [TR_SOMEIP_00251]
public class SDMessage extends Message {

	private final static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public SDMessage(int sessionId) {
		// See [TR_SOMEIP_00250]
		setServiceId(0xFFFF);
		setMethodId(0x8100);
		setProtocolVersion(0x01);
		setInterfaceVersion((byte) 0x01);
		setMessageType(Message.NOTIFICATION);
		setReturnCode(0x00);
		setClientId(0);
		setSessionId(0);
		setSessionId(sessionId);
		setProtocolVersion(0x01);
	}

	// EntryType [TR_SOMEIP_00268],
	final static int FIND_SERVICE = 0x00;
	final static int OFFER_SERVICE = 0x01;
	final static int SUBSCRIBE = 0x06;
	final static int SUBSCRIBE_ACK = 0x07;

	// OptionType
	final static int IPV4_ENDPOINT_OPTION = 0x04; // [TR_SOMEIP_00309]
	final static int IPV6_ENDPOINT_OPTION = 0x06; // [TR_SOMEIP_00318]

	// Transport protocols for the endpoint options ([TR_SOMEIP_00307] / [TR_SOMEIP_00316])
	public final static int TCP = 0x06;
	public final static int UDP = 0x11;

	public int fromBytes(byte[] data) {

		// Set payload
		setData(data);

		// Deserialize message
		serviceId = deserializeShort();
		methodId = deserializeShort();
		length = deserializeInt();
		clientId = deserializeShort();
		sessionId = deserializeShort();
		protocolVersion = deserializeByte();
		interfaceVersion = deserializeByte();
		messageType = deserializeByte();
		returnCode = deserializeByte();

		// Deserialize SDMessage
		flags = deserializeByte();

		// Reserved bytes
		deserializeByte();
		deserializeByte();
		deserializeByte();

		// Read entries array
		int entriesArraySize = deserializeInt();
		log.fine("EntriesArraySize: " + entriesArraySize);

		if(entriesArraySize > 0) {

			byte[] raw1 = deserializeBytes(entriesArraySize);
			byte entryType = raw1[0];

			// Some logging
			if(entryType == FIND_SERVICE) {
				log.fine("EntryType FIND_SERVICE");
			}
			else if(entryType == OFFER_SERVICE) {
				log.fine("EntryType OFFER_SERVICE");
			}
			else
				log.fine("EntryType " + entryType);

			while(entriesArraySize > 0) {
				SDServiceEntryType sde = new SDServiceEntryType();
				entriesArraySize -= sde.fromBytes(raw1);

				log.fine(sde.toString());
				entries.add(sde);
			}
		}

		// Read options array
		int optionsArrayLength = deserializeInt();
		log.fine("OptionsArrayLength: " + optionsArrayLength);

		if(optionsArrayLength > 0) {
			byte[] raw2 = deserializeBytes(optionsArrayLength);
			byte optionType = raw2[0];

			// Some logging
			if(optionType != IPV4_ENDPOINT_OPTION) {
				log.fine("OptionType IPV4_ENDPOINT_OPTION");
			}
			if(optionType != IPV6_ENDPOINT_OPTION) {
				log.fine("OptionType IPV6_ENDPOINT_OPTION");
			}

			while(optionsArrayLength > 0) {

				SDIPv4EndpointOption sdo = new SDIPv4EndpointOption();
				optionsArrayLength -= sdo.fromBytes(raw2);

				log.fine(sdo.toString());
				options.add(sdo);
			}
		}

		return getOffset();
	}

	public byte[] toBytes() {
		setOffset(0);

		// Message
		serializeShort(serviceId);
		serializeShort(methodId);

		// Save length offset as it's filled in later
		lengthOffset = getOffset();
		serializeInt(0);

		serializeShort(clientId);
		serializeShort(sessionId);
		serializeByte(protocolVersion);
		serializeByte(interfaceVersion);
		serializeByte(messageType);
		serializeByte(returnCode);

		serializeByte(flags);

		// SDMessage

		// Reserved bytes
		serializeByte((byte) 0);
		serializeByte((byte) 0);
		serializeByte((byte) 0);

		// Store length field offset
		// Serialize the data and store the end offset
		int entriesArrayLengthPos = getOffset();
		serializeInt(0);

		int entriesArrayLengthOffset = getOffset();
		for(SDServiceEntryType e : entries)
			serializeBytes(e.toBytes());

		int entriesArraySize = getOffset() - entriesArrayLengthOffset;

		// Store length field offset
		// Serialize the data and store the end offset
		int optionsArrayLengthPos = getOffset();
		serializeInt(0);

		int optionsArrayLengthOffset = getOffset();
		for(SDIPv4EndpointOption o : options)
			serializeBytes(o.toBytes());

		int optionsArraySize = getOffset() - optionsArrayLengthOffset;

		int end = getOffset();

		// Patch up the size fields when we're done
		// Serializing both arrays
		setOffset(entriesArrayLengthPos);
		serializeInt(entriesArraySize);

		setOffset(optionsArrayLengthPos);
		serializeInt(optionsArraySize);

		// Message length field
		// Patch the length field, then jump back to the end
		int length = end - 8; // Omit first two ints [TR_SOMEIP_00042]
		setOffset(lengthOffset);
		serializeInt(length);

		setOffset(end);

		// Trim
		byte[] trim = new byte[getOffset()];
		System.arraycopy(getData(), 0, trim, 0, getOffset());

		return trim;
	}

	public String toString() {
		return super.toString() + "SDMessage Flags: " + (int) (0xFF & flags) + ", Entries: "
				+ entries.size() + ", Options: " + options.size() + "\n";
	}

	// SD Message header [TR_SOMEIP_00251]
	private byte flags = 0;

	private List<SDServiceEntryType> entries = new ArrayList<SDServiceEntryType>();
	public void addEntry(SDServiceEntryType e) {
		entries.add(e);
	}

	private List<SDIPv4EndpointOption> options = new ArrayList<SDIPv4EndpointOption>();
	public void addOption(SDIPv4EndpointOption o) {
		options.add(o);
	}

	public SDIPv4EndpointOption getIPv4EndpointOption() {
		// TODO only one?
		return  options.get(0);
	}

	public SDServiceEntryType getServiceEntryType() {
		// TODO only one?
		return  entries.get(0);
	}
}
