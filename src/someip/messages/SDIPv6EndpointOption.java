/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.messages;

import java.net.InetAddress;
import java.net.UnknownHostException;

// See [TR_SOMEIP_00318]
public class SDIPv6EndpointOption extends Payload {

	public SDIPv6EndpointOption() {
		// See [TR_SOMEIP_00318]
		setLength(0x0015);
		setType(0x06);
	}

	public byte[] toBytes() {
		serializeShort(Length);
		serializeByte(Type);
		serializeByte((byte) 0x00);
		serializeBytes(IPv6Address);
		serializeByte((byte) 0x00);
		serializeByte(TransportProtocol);
		serializeInt(TransportProtocolPortNumber);

		// Trim
		byte[] trim = new byte[getOffset()];
		System.arraycopy(getData(), 0, trim, 0, getOffset());

		return trim;
	}

	public int fromBytes(byte[] data) {
		setData(data);

		Length = deserializeShort();
		Type = deserializeByte();
		deserializeByte();
		IPv6Address = deserializeBytes(16);
		deserializeByte();
		TransportProtocol = deserializeByte();
		TransportProtocolPortNumber = deserializeShort();

		return getOffset();
	}

	public String toString() {
		return "Service Option " + "Type: " + (int) (0xFF & Type) + ", IPv6Address: "
				+ IPv6Address + ""
				+ ", TransportProtocolPortNumber: " + TransportProtocolPortNumber
				+ ", TransportProtocol: " + TransportProtocol;
	}

	private void setType(int Type) {
		this.Type = (byte) Type;
	}

	private void setLength(int Length) {
		this.Length = (short) Length;
	}

	public byte[] getIPv6Address() {
		return IPv6Address;
	}

	public String getIPv6AddressString() {
		try {
			InetAddress address = InetAddress.getByAddress(IPv6Address);
			return address.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void setIPv6Address(String IPv6Address) {
		try {
			InetAddress addr = InetAddress.getByName(IPv6Address);
			byte[] IPv6AddressBytes = addr.getAddress();
			this.IPv6Address = IPv6AddressBytes;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public void setIPv6Address(byte[] IPv6Address) {
		this.IPv6Address = IPv6Address;
	}

	public byte getTransportProtocol() {
		return TransportProtocol;
	}

	public void setTransportProtocol(byte transportProtocol) {
		this.TransportProtocol = transportProtocol;
	}

	public int getTransportProtocolPortNumber() {
		return TransportProtocolPortNumber;
	}

	public void setTransportProtocolPortNumber(short transportProtocolPortNumber) {
		this.TransportProtocolPortNumber = transportProtocolPortNumber;
	}

	// Shall be set to 0x0009.
	private short Length;

	// Shall be set to 0x24.
	private byte Type;

	// Shall transport the unicast IP-Address of SOME/IP as
	// eight bytes.
	private byte[] IPv6Address;

	// Shall be set to the transport layer protocol
	// (ISO/OSI layer 4) based on the IANA/IETF types (0x06: TCP, 0x11: UDP).
	private byte TransportProtocol;

	// Shall be set to the transportlayer port of SOME/IP-SD (e.g. 30490).
	private int TransportProtocolPortNumber;
}
