/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.messages;

public class Message extends Payload {

	public Message() {
		// Default initialization
		this.clientId = 0x01; // Needs to be unique (and apparently != 0 for vSomeIP)
		this.protocolVersion = 0x01; // Currently not used but must be 0x01
		this.interfaceVersion = (byte) interfaceVersion;
	}

	protected int lengthOffset = 0;

	public String toString() {
		return super.toString() + "Message ServiceId: " + Integer.toHexString(0xFFFF & serviceId) +
				", MethodId: " + Integer.toHexString(0xFFFF & methodId)
				+ ", MessageType: " + Integer.toHexString(messageType) + "\n";
	}

	public byte[] toBytes() {
		setOffset(0);

		serializeShort(serviceId);
		serializeShort(methodId);

		// Save length offset as it's filled in later
		lengthOffset = getOffset();
		serializeInt(0);

		serializeShort(clientId);
		serializeShort(sessionId);
		serializeByte(protocolVersion);
		serializeByte(interfaceVersion);
		serializeByte(messageType);
		serializeByte(returnCode);

		serializeBytes(messagePayload);

		int length = getOffset() - 8; // Omit first two ints [TR_SOMEIP_00042]

		// Patch the length field, then jump back to the end
		int end = getOffset();
		setOffset(lengthOffset);
		serializeInt(length);
		setOffset(end);

		// Trim
		byte[] trim = new byte[getOffset()];
		System.arraycopy(getData(), 0, trim, 0, getOffset());

		return trim;
	}

	public int fromBytes(byte[] data) {
		setData(data);

		serviceId = deserializeShort();
		methodId = deserializeShort();
		length = deserializeInt();
		clientId = deserializeShort();
		sessionId = deserializeShort();
		protocolVersion = deserializeByte();
		interfaceVersion = deserializeByte();
		messageType = deserializeByte();
		returnCode = deserializeByte();

		int payloadSize = length - 8; // [TR_SOMEIP_00042] Part of the header is contained in the length
		messagePayload = deserializeBytes(payloadSize);

		return getOffset();
	}

	public void setServiceId(int serviceId) {
		this.serviceId = (short) serviceId;
	}


	public short getServiceId() {
		return this.serviceId;
	}

	protected void setProtocolVersion(int protocolVersion) {
		this.protocolVersion = (byte) protocolVersion;
	}

	/*
	 * Used only for request/response.
	 * First bit set to zero indicates a method [TR_SOMEIP_00038]
	 */
	protected void setMethodId(int methodId) {
		// TODO Should mask highest bit (0xEFFF) but apparently
		// people also use the highest bit in method identifiers
		// -> tolerate abuse against specification
		this.methodId = (short)(methodId & 0xFFFF);
	}

	public short getMethodId() {
		return this.methodId;
	}

	/*
	 * Used only for publish/subscribe.
	 * First bit set to one indicates an event (Pub/Sub) [TR_SOMEIP_00040]
	 */
	protected void setEventId(int methodId) {
		this.methodId = (short)(methodId & 0xFFFF);
	}

	protected void setClientId(int clientId) {
		this.clientId = (short)(clientId);
	}

	protected void setSessionId(int sessionId) {
		this.sessionId = (short)(sessionId);
	}

	public int getRequestId() {
		return (int) ((clientId << 16) | sessionId);
	}

	public void setRequestId(int requestId) {
		this.clientId = (short) ((requestId >> 16) & 0xFFFF);
		this.sessionId = (short) (requestId & 0xFFFF);
	}

	/**
	 * Setting the messageId indicates that you are not using
	 * request/response or publish/subscribe paradigms
	 */
	void setMessageId(int messageId) {
		this.serviceId = (short) (messageId & 0xFFFF0000);
		this.methodId = (short) ((messageId & 0x0000FFFF) << 8);
	}

	public void setInterfaceVersion(byte interfaceVersion) {
		this.interfaceVersion = interfaceVersion;
	}

	public byte getInterfaceVersion() {
		return this.interfaceVersion;
	}

	public void setMessageType(final int messageType) {
		this.messageType = (byte) messageType;
	}

	public byte getMessageType() {
		return this.messageType;
	}

	protected void setLength(int i) {
		length = 0;
	}

	protected void setReturnCode(int i) {
		returnCode = 0;
	}

	public void setMessagePayload(byte[] payload) {
		this.messagePayload = payload;
	}

	public byte[] getMessagePayload() {
		return this.messagePayload;
	}

	public short getSessionId() {
		return sessionId;
	}

	// ReturnCodes [TR_SOMEIP_00191]
	final static int E_OK = 0x00;
	final static int E_NOT_OK = 0x01;
	final static int E_UNKNOWN_SERVICE = 0x02;
	final static int E_UNKNOWN_METHOD = 0x03;
	final static int E_NOT_READY = 0x04;
	final static int E_NOT_REACHABLE = 0x05;
	final static int E_TIMEOUT = 0x06;
	final static int E_WRONG_PROTOCOL_VERSION = 0x07;
	final static int E_WRONG_INTERFACE_VERSION = 0x08;
	final static int E_MALFORMED_MESSAGE = 0x09;
	final static int E_WRONG_MESSAGE_TYPE = 0x0a;
	// 0x0b - Reserved

	public void returnCode(final int returnCode) {
		this.returnCode = (byte) returnCode;
	}

	// MessageType [TR_SOMEIP_00055]
	public final static int REQUEST = 0x00;
	public final static int REQUEST_NO_RETURN = 0x01;
	public final static int NOTIFICATION = 0x02;
	public final static int REQUEST_ACK = 0x40;
	public final static int REQUEST_NO_RETURN_ACK = 0x41;
	public final static int NOTIFICATION_ACK = 0x42;
	public final static int RESPONSE = 0x80;
	public final static int ERROR = 0x81;
	public final static int RESPONSE_ACK = 0xC0;
	public final static int ERROR_ACK = 0xC1;

	public static String messageTypeName(byte messageType) {
		switch((int) messageType) {
		case Message.REQUEST:
			return "REQUEST";
		case Message.REQUEST_NO_RETURN:
			return "REQUEST_NO_RETURN";
		case Message.NOTIFICATION:
			return "NOTIFICATION";
		case Message.REQUEST_ACK:
			return "REQUEST_ACK";
		case Message.REQUEST_NO_RETURN_ACK:
			return "REQUEST_NO_RETURN_ACK";
		case Message.NOTIFICATION_ACK:
			return "NOTIFICATION_ACK";
		case Message.RESPONSE:
			return "RESPONSE";
		case Message.ERROR:
			return "ERROR";
		case Message.RESPONSE_ACK:
			return "RESPONSE_ACK";
		case Message.ERROR_ACK:
			return "ERROR_ACK";
		}

		return "Invalid messageType";
	}

	public void messageNotify() {
		setMessageType(Message.NOTIFICATION);
		setLength(0);
	}

	public void messageRequestNoReturn(int methodId, byte[] payload) {
		setMessageType(Message.REQUEST_NO_RETURN);
		setReturnCode(0);
		setMethodId(methodId);

		setMessagePayload(payload);
	}

	public void messageRequestNoReturn(int methodId) {
		setMessageType(Message.REQUEST_NO_RETURN);
		setReturnCode(0);

		setMethodId(methodId);
	}

	public void messageRequest(int methodId) {
		setMessageType(Message.REQUEST);
		setReturnCode(0);

		setMethodId(methodId);
	}

	// Message header [TR_SOMEIP_00031]
	protected short serviceId; // TODO [TR_SOMEIP_00038]
	protected short methodId;
	protected int length;
	protected short clientId;
	protected short sessionId;
	protected byte protocolVersion;
	protected byte interfaceVersion;
	protected byte messageType;
	protected byte returnCode;

	private byte[] messagePayload;
}
