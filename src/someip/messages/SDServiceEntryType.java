/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.messages;

public class SDServiceEntryType extends Payload {

	public byte[] toBytes() {
		serializeByte(type);
		serializeByte(IndexFirstOptionRun);
		serializeByte(IndexSecondOptionRun);
		serializeByte((byte) (NumberOfOptions1 & (NumberOfOptions2 << 4)));
		serializeShort(serviceId);
		serializeShort(instanceId);

		int tmp = (MajorVersion >> 24) | (TTL & 0x0FFF);
		serializeInt(tmp);

		serializeInt(MinorVersion);

		// Trim
		byte[] trim = new byte[getOffset()];
		System.arraycopy(getData(), 0, trim, 0, getOffset());

		return trim;
	}

	public int fromBytes(byte[] data) {
		setData(data);

		type = deserializeByte();
		IndexFirstOptionRun = deserializeByte();
		IndexSecondOptionRun = deserializeByte();
		byte NumberOfOptions = deserializeByte();
		NumberOfOptions1 = (byte) (NumberOfOptions & 0x0F);
		NumberOfOptions2 = (byte) ((NumberOfOptions & 0x0F) >> 4);
		serviceId = deserializeShort();
		instanceId = deserializeShort();

		int tmp = deserializeInt();
		MajorVersion = (byte) ((tmp & 0xF000) >> 24);
		TTL = (byte) (tmp & 0x0FFF);
		MinorVersion = deserializeInt();

		return getOffset();
	}

	public String toString() {
		return "Service Entry " + "Type: " + (int) (0xFF & type) + ", ServiceId: "
				+ serviceId + ", InstanceId: " + instanceId;
	}

	public short getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(short serviceId) {
		this.serviceId = serviceId;
	}

	public void setInstanceId(short instanceId) {
		this.instanceId = instanceId;
	}

	public short getInstanceId() {
		return this.instanceId;
	}

	// encodes FindService (0x00) and OfferService (0x01).
	private byte type;

	// Index of this runs first option in the option array.
	private byte IndexFirstOptionRun;

	// Index of this runs second option in the option array.
	private byte IndexSecondOptionRun;

	// Describes the number of options the first option run uses.
	private byte NumberOfOptions1; // actually a nibble

	// Describes the number of options the second option run uses.
	private byte NumberOfOptions2; // actually a nibble

	// Describes the Service ID of the Service or Service-Instance this entry is concerned with.
	private short serviceId = 0;

	// Describes the Service Instance ID of the Service Instance
	// this entry is concerned with or is set to 0xFFFF if all service instances of a service
	// are meant.
	private short instanceId = 0;

	// Encodes the major version of the service (instance).
	private byte MajorVersion;

	// Describes the lifetime of the entry in seconds.
	private int TTL; // actually 24bit

	// Encodes the minor version of the service.
	private int MinorVersion;
}
