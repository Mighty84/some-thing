/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.rpc;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Vector;
import java.util.logging.Logger;

import someip.messages.SDIPv4EndpointOption;
import someip.messages.SDMessage;
import someip.messages.SDServiceEntryType;

public class ServiceRegistry implements Runnable {

	private final static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private Vector<ServiceStub> serviceStubList = new Vector<ServiceStub>();
	private Vector<ServiceProxy> serviceProxyList = new Vector<ServiceProxy>();

	private InetAddress multicastAddress = null;
	private MulticastSocket socket = null;

	private int promoteInterval;

	public ServiceRegistry(String multicastGroup, int port, int promoteInterval) {
		// Register in multicast group and configure the socket
		// to blocking receive
		try {
			this.promoteInterval = promoteInterval;

			multicastAddress = InetAddress.getByName(multicastGroup);
			socket = new MulticastSocket(port);
			socket.setReuseAddress(true);
			socket.setSoTimeout(50);
			socket.joinGroup(multicastAddress);

			// Inverted! Set to false in order to use services locally /
			// promote services to localhost
			socket.setLoopbackMode(false);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean handleServiceDiscoveryMessage() {
		try {
			byte[] rxbuf = new byte[8192];

			DatagramPacket packet = new DatagramPacket(rxbuf, rxbuf.length);
			socket.receive(packet);

			log.fine("Received a packet with length: " + packet.getLength());

			// Trim rx buffer
			byte[] trim = new byte[packet.getLength()];
			System.arraycopy(packet.getData(), 0, trim, 0, packet.getLength());

			SDMessage sdm = new SDMessage(1);
			sdm.fromBytes(trim);

			SDIPv4EndpointOption sdo = sdm.getIPv4EndpointOption();
			SDServiceEntryType sde = sdm.getServiceEntryType();

			log.fine(sdm.toString());

			log.info("Attempting to register service for address: " + sdo.getIPv4AddressString()
			+ ", port: " + sdo.getTransportProtocolPortNumber()
			+ ", serviceId: " + sde.getServiceId()
			+ ", interfaceVersion: " + sdm.getInterfaceVersion()
			+ ", transportProtocol: " + sdo.getTransportProtocol()
			+ ", instanceId: " + sde.getInstanceId());

			// Set the address
			for(ServiceProxy s : serviceProxyList) {

				if(s.getServiceId() == sde.getServiceId()
					&& s.getInstanceId() == sde.getInstanceId()
					&& s.getTransportProtocol() == sdo.getTransportProtocol()
					&& s.getInterfaceVersion() == sdm.getInterfaceVersion()) {

					s.setAddress(new InetSocketAddress(sdo.getIPv4AddressString(), sdo.getTransportProtocolPortNumber()));
					s.setConnectionState(true);
					return true;
				}
			}

			// No proxy to handle service registered
			log.fine("No service proxy registered to interface with the offered service");

		} catch (SocketTimeoutException e) {
			return false; // We're fine. More luck next time.
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	private void promoteService(ServiceStub service) {

		SDMessage sdm = new SDMessage(service.getAddress().getPort());
		sdm.setInterfaceVersion(service.getInterfaceVersion());

		SDServiceEntryType sde = new SDServiceEntryType();
		sde.setServiceId(service.getServiceId());
		sde.setInstanceId(service.getInstanceId());

		SDIPv4EndpointOption sdip = new SDIPv4EndpointOption();
		sdip.setIPv4Address(service.getAddress().getHostName());
		sdip.setTransportProtocolPortNumber((short) service.getAddress().getPort());
		sdip.setTransportProtocol(service.getTransportProtocol());

		sdm.addEntry(sde);
		sdm.addOption(sdip);

		byte[] txmsg = sdm.toBytes();

		DatagramPacket op = new DatagramPacket(txmsg, txmsg.length, multicastAddress, socket.getLocalPort());
		op.setAddress(multicastAddress);

		log.fine("Sending SDMessage to multicast-address: " + multicastAddress.getHostAddress());

		try {
			socket.send(op);

			log.info("Promoted service with serviceId: " + service.getServiceId()
				+ ", interfaceVersion: " + service.getInterfaceVersion());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void registerService(ServiceProxy service) {

		log.info("Registered a service proxy for serviceId: " + service.getServiceId()
		+ " interfaceVersion: " + service.getInterfaceVersion()
		+ " transportProtocol: " + service.getTransportProtocol()
		+ " instanceId: " + service.getInstanceId());

		serviceProxyList.add(service);
	}

	public void registerService(ServiceStub service) {

		log.info("Registered a service stub for serviceId: " + service.getServiceId()
		+ " interfaceVersion: " + service.getInterfaceVersion()
		+ " transportProtocol: " + service.getTransportProtocol());

		serviceStubList.add(service);
	}

	public void run() {
		try {
			do {
				// Handle all incoming service-discovery messages
				// until there are none left
				while(handleServiceDiscoveryMessage());

				// Now promote the services we have in our registry
				for(ServiceStub s : serviceStubList)
					promoteService(s);

				Thread.sleep(promoteInterval);
			} while (true);

		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
}
