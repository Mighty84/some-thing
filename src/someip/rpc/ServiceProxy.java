/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.rpc;

import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import someip.messages.Message;
import someip.messages.SDMessage;

public class ServiceProxy {

	private final static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private InetSocketAddress address = null;
	private short serviceId = 0;
	private byte interfaceVersion = 0;
	private byte transportprotocol = 0;
	private short instanceId = 0;
	private boolean connected = false;

	// Proxy configured for either UDP or TCP
	private DatagramSocket udpSocket = null;
	private Socket tcpSocket = null;

	public ServiceProxy(short serviceId, byte interfaceVersion, byte transportprotocol, short instanceId) {

		this.serviceId = serviceId;
		this.interfaceVersion = interfaceVersion;
		this.transportprotocol = transportprotocol;
		this.instanceId = instanceId;

		try {
			if(transportprotocol == SDMessage.UDP)
				udpSocket = new DatagramSocket();

		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public InetSocketAddress getAddress() {
		return this.address;
	}

	public void setAddress(InetSocketAddress address) {
		this.address = address;
	}

	public short getServiceId() {
		return this.serviceId;
	}

	public byte getInterfaceVersion() {
		return this.interfaceVersion;
	}

	public byte getTransportProtocol() {
		return this.transportprotocol;
	}

	public short getInstanceId() {
		return this.instanceId;
	}

	public boolean isConnected() {
		return this.connected;
	}

	public void setConnectionState(boolean connected) {

		this.connected = connected;

		log.info("Updated connection state connected: " + connected);
	}

	protected void callServiceRequestNoResponse(short serviceId, short methodId, byte[] payload) {

		// Generate the call message
		Message m = new Message();
		m.setServiceId(serviceId); // The service we are calling
		m.setMessageType(m.REQUEST_NO_RETURN);
		m.messageRequestNoReturn(methodId, payload);
		byte[] msg = m.toBytes();

		try {
			if(transportprotocol == SDMessage.UDP) {
				if(address == null) {
					log.warning("Service stub for serviceId: "
							+ serviceId + ", instanceId: " + instanceId  + ", transportProtocol: UDP  not found. The proxy cannot send to a stub without address.");
					return;
				}

				// See [TR_SOMEIP_00061]
				if(m.getMessagePayload().length > 1400)
					log.warning("For SOME/IP messages sent with the UDP transportProtocol, "
							+ "payload shall not be > 1400 bytes. Behaviour undefined.");

				udpSocket.send(new DatagramPacket(msg, msg.length, address.getAddress(), address.getPort()));

				log.info("Sent a UDP packet with length: " + msg.length);

			} else {
				if(address == null) {
					log.warning("Service stub for serviceId: "
							+ serviceId + ", instanceId: " + instanceId + ", transportProtocol: TCP  not found. The proxy cannot connect to a stub without address.");
					return;
				}
				if(tcpSocket == null || !tcpSocket.isConnected()) {
					tcpSocket = new Socket();
					tcpSocket.connect(address);
					tcpSocket.setTcpNoDelay(true); // See [TR_SOMEIP_00148]
					tcpSocket.setSoTimeout(50); // Non-blocking write
				}

				OutputStream os = tcpSocket.getOutputStream();
				os.write(msg);
				os.flush();

				log.info("Sent a TCP packet with length: " + msg.length);
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			try {
				log.fine("Remote connection to stub closed.");
				tcpSocket.close();
				tcpSocket = null;
			} catch (Exception ee) {
				ee.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void callServiceRequest(short serviceId, short methodId, byte[] payload) {
		callServiceRequestNoResponse(serviceId, methodId, payload);
	}
}
