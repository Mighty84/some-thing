/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package someip.rpc;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import someip.messages.Message;
import someip.messages.Payload;
import someip.messages.SDMessage;

abstract public class ServiceStub {

	private final static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private InetSocketAddress address = null;
	private short serviceId = 0;
	private short instanceId = 0;
	private byte interfaceVersion = 0;
	private byte transportProtocol = 0;

	private static final int BUFSIZE = 1024*64;
	private static final int READTIMEOUT = 50;
	private static final int TCPDISCONNECT = 100; // Close socket after N read-timeouts
	private int tcpTimeoutCounter = 0;

	// Stub configured for either UDP or TCP
	private DatagramSocket udpSocket = null;
	private ServerSocket tcpServerSocket = null;
	private Socket tcpSocket = null;

	public ServiceStub(InetSocketAddress address, short serviceId, byte interfaceVersion, byte transportProtocol, short instanceId) {

		this.address = address;
		this.serviceId = serviceId;
		this.interfaceVersion = interfaceVersion;
		this.transportProtocol = transportProtocol;
		this.instanceId = instanceId;

		// If no address was set, use the default host-address
		if(address.getAddress().getHostAddress().equals("0.0.0.0")) {
			try {
				String hostIp = InetAddress.getLocalHost().getHostAddress();

				if(address.getPort() == 0)
					this.address = new InetSocketAddress(hostIp, serviceId);
				else
					this.address = new InetSocketAddress(hostIp, address.getPort());

			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}

		// Create either a UDP or a TCP socket
		if(transportProtocol == SDMessage.UDP) {
			try {
				udpSocket = new DatagramSocket(address.getPort());
				udpSocket.setSoTimeout(50);
			} catch (SocketException e) {
				e.printStackTrace();
			}
		} else {
			try {
				tcpServerSocket = new ServerSocket();
				tcpServerSocket.setSoTimeout(READTIMEOUT);
				tcpServerSocket.bind(new InetSocketAddress(address.getPort()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public InetSocketAddress getAddress() {
		return this.address;
	}

	public short getServiceId() {
		return this.serviceId;
	}

	public byte getInterfaceVersion() {
		return this.interfaceVersion;
	}

	public byte getTransportProtocol() {
		return this.transportProtocol;
	}

	public short getInstanceId() {
		return instanceId;
	}

	abstract public void handleMessage(byte messageType, int methodId, byte[] payload);

	// Receive a message and delegate it to the message handler
	// Currently implemented blocking with timeout for both UDP and TCP ports
	// To be called repeatedly until it returns false
	public boolean handleMessage() {

		try {
			byte[] rxbuf = new byte[BUFSIZE];
			byte[] trim = null;

			if(transportProtocol == SDMessage.UDP) {
				try {
					DatagramPacket packet = new DatagramPacket(rxbuf, rxbuf.length);
					udpSocket.receive(packet);

					// Trim rx buffer
					trim = new byte[packet.getLength()];
					System.arraycopy(packet.getData(), 0, trim, 0, packet.getLength());

					log.info("Received an answer on bound UDP port with length: " + packet.getLength());

					Message m = new Message();
					m.fromBytes(trim);
					handleMessage(m.getMessageType(), m.getMethodId(), m.getMessagePayload());

				} catch (SocketTimeoutException e) {
					return false; // We're fine. Nothing received. More luck next time.
				}

			} else {
				if(tcpSocket == null || !tcpSocket.isConnected()) {
					try {
						tcpSocket = tcpServerSocket.accept();

					} catch(SocketTimeoutException e) {

						log.warning("Proxy for serviceId: " + this.serviceId + ", instanceId: " + instanceId + " not connected");
						return false;
					}

					tcpSocket.setSoTimeout(READTIMEOUT);
					tcpSocket.setSoLinger(true, 0);
					tcpSocket.setTcpNoDelay(true); // See [TR_SOMEIP_00148]
				}

				try {
					InputStream is = tcpSocket.getInputStream();
					int readBytes = is.read(rxbuf, 0, 8);

					tcpTimeoutCounter = 0;

					if(readBytes < 8)
						throw new SocketException("Unable to read expected SOME/IP header");

					Payload p = new Payload();
					p.fromBytes(rxbuf);

					int serviceId = p.deserializeShort(); // Ignore
					p.deserializeShort(); // Ignore
					int payloadLength = p.deserializeInt();

					log.info("Reading payload from serviceId: " + serviceId +  " with length: "+ (payloadLength+8));

					int offset = 8;
					while(offset < payloadLength+8)
							offset += is.read(rxbuf, offset, payloadLength+8-offset);

					Message m = new Message();
					m.fromBytes(rxbuf);
					handleMessage(m.getMessageType(), m.getMethodId(), m.getMessagePayload());
					log.info("Received a payload on bound TCP port with length: " + offset);
					return true;

				} catch (SocketException e) {
					tcpSocket.close();
					tcpSocket = null;
					log.warning("Connection closed");

					e.printStackTrace();
					return false;

				} catch (SocketTimeoutException e) {
					tcpTimeoutCounter += 1;

					if(tcpTimeoutCounter > TCPDISCONNECT) {
						tcpTimeoutCounter = 0;

						//tcpSocket.close();
						//tcpSocket = null;
						log.warning("Remote client with serviceId: " + serviceId + ", instanceId: " + instanceId + " disconnected");
					}

					return false; // We're fine. Nothing received. More luck next time.
				}
			}

		} catch(IOException e) {
			e.printStackTrace();
		}

		return false;
	}
}
