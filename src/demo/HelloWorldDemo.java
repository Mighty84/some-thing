/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package demo;

import java.util.logging.Level;
import java.util.logging.Logger;

import someip.rpc.ServiceRegistry;

public class HelloWorldDemo {

	private final static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static void main(String[] args) {

		log.setLevel(Level.ALL);

		// Create and start a service registry
		ServiceRegistry serviceRegistry = new ServiceRegistry("224.224.224.0", 30490, 1000);
		(new Thread(serviceRegistry)).start();

		// Create a service implementation that will
		// handle all called service methods
		HelloWorldServiceImpl helloStub = new HelloWorldServiceImpl();
		(new Thread(helloStub)).start();
		serviceRegistry.registerService(helloStub);

		// Create a service proxy
		HelloWorldServiceProxy helloProxy = new HelloWorldServiceProxy();
		serviceRegistry.registerService(helloProxy);

		// Call the hello method periodically
		while(true) {
			try {
				helloProxy.hello("Test string".getBytes());
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
