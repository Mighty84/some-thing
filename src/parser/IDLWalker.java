package parser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

public class IDLWalker extends IDLBaseListener {

	private static final Logger log = Logger.getLogger(IDLBaseListener.class.getName());

	private FileWriter stub = null;
	private FileWriter proxy = null;

	private int serviceId = 0;
	private byte interfaceVersion = 0;
	private String transportProtocol = "";

	private Vector<Method> methodList = new Vector<Method>();

	private class Method {
		private String messageType;
		private String name;
		private Vector<Parameter> parameterList = new Vector<Parameter>();

		public Method(String messageType, String name) {
			this.messageType = messageType;
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public String getMessageType() {
			return messageType;
		}

		public void addParameter(Parameter p) {
			parameterList.add(p);
		}

		public Vector<Parameter> getParameterList() {
			return parameterList;
		}
	}

	private class Parameter {
		private String type;
		private String name;

		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}

		public Parameter(String type, String name) {
			this.type = type;
			this.name = name;
		}
	}

	public String someipToJavaParameter(String someipType, String name) {
		if(someipType.equals("uint8"))
			return "byte " + name;
		if(someipType.equals("uint16"))
			return "short " + name;
		if(someipType.equals("uint32"))
			return "int " + name;
		if(someipType.equals("uint64"))
			return "long " + name;
		if(someipType.equals("sint8"))
			return "byte " + name;
		if(someipType.equals("sint16"))
			return "short " + name;
		if(someipType.equals("sint32"))
			return "int " + name;
		if(someipType.equals("sint64"))
			return "long " + name;
		if(someipType.equals("bytes"))
			return "byte[] " + name;

		return someipType + " not implemented " + " for " + name;
	}

	public String someipToPayloadSerialize(String someipType, String name) {
		if(someipType.equals("uint8"))
			return "p.serializeByte(" + name + ");\n";
		if(someipType.equals("uint16"))
			return "p.serializeShort(" + name + ");\n";
		if(someipType.equals("uint32"))
			return "p.serializeInt(" + name + ");\n";
		if(someipType.equals("uint64"))
			return "p.serializeLong(" + name + ");\n";
		if(someipType.equals("sint8"))
			return "p.serializeByte(" + name + ");\n";
		if(someipType.equals("sint16"))
			return "p.serializeShort(" + name + ");\n";
		if(someipType.equals("sint32"))
			return "p.serializeInt(" + name + ");\n";
		if(someipType.equals("sint64"))
			return "p.deserializeLong()";
		if(someipType.equals("bytes"))
			return "p.serializeInt(" + name + ".length); p.serializeBytes(" + name + ");\n";

		return someipType + " not implemented " + " for " + name;
	}

	public String someipToPayloadDeserialize(String someipType, String name) {
		if(someipType.equals("uint8"))
			return "p.deserializeByte()";
		if(someipType.equals("uint16"))
			return "p.deserializeShort()";
		if(someipType.equals("uint32"))
			return "p.deserializeInt()";
		if(someipType.equals("uint64"))
			return "p.deserializeLong()";
		if(someipType.equals("sint8"))
			return "p.deserializeByte()";
		if(someipType.equals("sint16"))
			return "p.deserializeShort()";
		if(someipType.equals("sint32"))
			return "p.deserializeInt()";
		if(someipType.equals("sint64"))
			return "p.deserializeLong()";
		if(someipType.equals("bytes")) {
			return "p.deserializeBytes(p.deserializeInt())";
		}

		return someipType + " not implemented " + " for " + name;
	}

	public String someipTransportProtocol(String name) {
		if(name.equals("UDP"))
			return "SDMessage.UDP";
		if(name.equals("TCP"))
			return "SDMessage.TCP";

		return name + " transportProtocol not implemented";
	}

	public void writeStub(String packageName, String name, int serviceId, byte interfaceVersion) throws IOException {
		String prolog =
			"package " + packageName + ";\n"
			+ "import java.net.InetSocketAddress;\n"
			+ "import java.util.logging.Logger;\n"
			+ "import someip.messages.Message;\n"
			+ "import someip.messages.Payload;\n"
			+ "import someip.messages.SDMessage;\n"
			+ "import someip.rpc.ServiceStub;\n"
			+ "\n"
			+ "abstract public class " + name + " extends ServiceStub {\n"
			+ "\n"
			+ "\tprivate final static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);\n"
			+ "\tprivate static final InetSocketAddress address = new InetSocketAddress(\"localhost\", " + serviceId + ");\n"
			+ "\tprivate static final short serviceId = " + serviceId + ";\n"
			+ "\tprivate static final byte interfaceVersion = " + interfaceVersion + ";\n"
			+ "\tprivate static final byte transportProtocol = " + someipTransportProtocol(transportProtocol) + ";\n"
			+ "\n"
			+ "\tpublic " + name + "() {\n"
			+ "\t\tsuper(address, serviceId, interfaceVersion, transportProtocol);\n"
			+ "\t}\n"
			+ "\n";

		stub.write(prolog);

		int i = 0;
		for(Method m : methodList) {
			String ms = "\tpublic static final short METHOD_" + m.getName() + "_CALL = " + i++ + ";\n";

			stub.write(ms);
		}

		stub.write("\n");

		for(Method m : methodList) {
			String params = "";
			for(Parameter p : m.getParameterList())
				params += someipToJavaParameter(p.getType(), p.getName()) + ", ";
			if(params.length() > 0)
				params = params.substring(0, params.length()-2);

			String ms = "\tabstract public void " + m.getName() + "(" + params + ");\n";

			stub.write(ms);
		}

		stub.write("\n");
		stub.write("\tpublic void handleMessage(byte messageType, int methodId, byte[] payload) {\n");

		for(Method m : methodList) {
			String deserialize = "";
			for(Parameter p : m.getParameterList())
				deserialize += someipToPayloadDeserialize(p.getType(), p.getName()) + ", ";
			if(deserialize.length() > 0)
				deserialize = deserialize.substring(0, deserialize.length()-2);

			String ms =
					  "\t\tif(messageType == Message." + m.getMessageType() + " && methodId == METHOD_" + m.getName() + "_CALL) {\n\n"
					+ "\t\t\tPayload p = new Payload();\n"
					+ "\t\t\tp.fromBytes(payload);\n"
					+ "\t\t\t" + m.getName() + "(" + deserialize + ");\n"
					+ "\t\t}\n";

			stub.write(ms);
		}

		stub.write("}\n\n;");
		stub.write("}");
		stub.flush();
	}

	public void writeProxy(String packageName, String proxyName, String stubName, int serviceId, byte interfaceVersion) throws IOException {
		String prolog =
			"package " + packageName + ";\n"
			+ "import java.util.logging.Logger;\n"
			+ "import someip.rpc.ServiceProxy;\n"
			+ "import someip.messages.Payload;\n"
			+ "import someip.messages.SDMessage;\n"
			+ "\n"
			+ "public class " + proxyName + " extends ServiceProxy {\n"
			+ "\n"
			+ "\tprivate final static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);\n"
			+ "\tprivate static final short serviceId = " + serviceId + ";\n"
			+ "\tprivate static final byte interfaceVersion = " + interfaceVersion + ";\n"
			+ "\tprivate static final byte transportProtocol = " + someipTransportProtocol(transportProtocol) + ";\n"
			+ "\n"
			+ "\tpublic " + proxyName + "() {\n"
			+ "\t\tsuper(serviceId, interfaceVersion, transportProtocol);\n"
			+ "\t}\n"
			+ "\n";

		proxy.write(prolog);

		for(Method m : methodList) {
			String params = "";
			String serialize = "";
			for(Parameter p : m.getParameterList()) {
				params += someipToJavaParameter(p.getType(), p.getName()) + ", ";
				serialize += "\t\t" + someipToPayloadSerialize(p.getType(), p.getName());
			}
			if(params.length() > 0)
				params = params.substring(0, params.length()-2);

			String ms =
					"\tpublic void "+ m.getName() + "(" + params + ") {\n"
					+ "\t\tif(!isConnected()) {\n"
					+ "\t\t\tlog.warning(\"Cannot call function. The proxy is not connected to the service stub.\");\n"
					+ "\t\t\treturn;\n"
					+ "\t\t}\n"
					+ "\n"
					+ "\t\tPayload p = new Payload();\n"
					+ serialize
					+ "\t\tcallServiceRequest(serviceId, " + stubName + ".METHOD_" + m.getName() + "_CALL, p.toBytes());\n"
					+ "\t};\n"
					+ "\n";

			proxy.write(ms);
		}

		proxy.write("}");
		proxy.flush();
	}

	public void exitService(IDLParser.ServiceContext ctx) {

		String packagePath = "src/";
		String packageName = "";
		for(int i = 0; i < ctx.pkg().IDENTIFIER().size()-1; i++) {
			packagePath += ctx.pkg().IDENTIFIER(i).getText() + "/";
			packageName += ctx.pkg().IDENTIFIER(i).getText() + ".";
		}
		packagePath = packagePath.substring(0, packagePath.length()-1);
		packageName = packageName.substring(0, packageName.length()-1);

		int i = ctx.pkg().IDENTIFIER().size();
		String serviceName = ctx.pkg().IDENTIFIER(i-1).getText();

		try {
			File pPath = new File(packagePath);
			pPath.mkdirs();

			String stubName = serviceName+"ServiceStub";
			File stubFile = new File(packagePath, stubName+".java");
			stubFile.createNewFile();
			stub = new FileWriter(stubFile);

			String proxyName = serviceName+"ServiceProxy";
			File proxyFile = new File(packagePath, proxyName+".java");
			proxyFile.createNewFile();
			proxy = new FileWriter(proxyFile);

			writeStub(packageName, stubName, serviceId, interfaceVersion);
			writeProxy(packageName, proxyName, stubName, serviceId, interfaceVersion);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void enterServiceId(IDLParser.ServiceIdContext ctx) {

		serviceId = Integer.parseInt(ctx.INTEGER().getText());
	}

	public void enterInterfaceVersion(IDLParser.InterfaceVersionContext ctx) {

		interfaceVersion = (byte) Integer.parseInt(ctx.INTEGER().getText());
	}

	public void enterTransportProtocol(IDLParser.TransportProtocolContext ctx) {

		transportProtocol = ctx.TRANSPORT_PROTOCOL().getText();
	}

	public void enterMessageItem(IDLParser.MessageItemContext ctx) {

		Method m = new Method(ctx.MESSAGETYPE().getText(), ctx.IDENTIFIER().getText());
		methodList.add(m);
	}

	public void exitParameterItem(IDLParser.ParameterItemContext ctx) {

		Parameter p = new Parameter(ctx.TYPE().getText(), ctx.IDENTIFIER().getText());
		methodList.lastElement().addParameter(p);
	}

	String structName = null;
	Vector<StructItem> structItems = new Vector<StructItem>();
	private class StructItem {
		String type;
		String name;

		StructItem(String type, String name) {
			this.type = type;
			this.name = name;
		}

		String getType() {
			return type;
		}

		String getName() {
			return name;
		}
	}

	private void writeStruct() throws IOException {
		String packagePath = "src/";

		File structFile = new File(packagePath, structName+".java");

		structFile.createNewFile();
		FileWriter struc = new FileWriter(structFile);

		struc.write("public class " + structName + " extends Payload {\n");

		for(StructItem s : structItems) {
			struc.write("\t" + someipToJavaParameter(s.getType(), s.getName()) + ";\n");
		}

		struc.write("\n");

		for(StructItem s : structItems) {
			String name = s.getName().substring(0,1).toUpperCase() + s.getName().substring(1);
			struc.write("\tpublic get" + name + "{\n");
			struc.write("\t\treturn " + s.getName() + ";\n");
			struc.write("\t};\n\n");
		}

		struc.write("}");
		struc.flush();
		struc.close();
	}

	public void enterStruct(IDLParser.StructContext ctx) {

		structName = ctx.IDENTIFIER().getText();
	}

	public void enterStructItem(IDLParser.StructItemContext ctx) {

		structItems.addElement(new StructItem(ctx.TYPE().getText(), ctx.IDENTIFIER().getText()));
	}

	public void exitStruct(IDLParser.StructContext ctx) {

		try {
			writeStruct();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
