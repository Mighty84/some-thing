/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package parser;

import java.util.logging.Logger;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class ParseIDL {

	private static final Logger log = Logger.getLogger(IDLBaseListener.class.getName());

	public static void main(String[] args) throws Exception {

		if(args.length == 0) {
			log.severe("No input file specified!");
			return;
		}

		IDLLexer lexer = new IDLLexer(new ANTLRFileStream(args[0]));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		IDLParser parser = new IDLParser(tokens);

		ParseTree tree = parser.service();
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(new IDLWalker(), tree);
	}
}
