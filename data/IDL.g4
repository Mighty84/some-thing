/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
grammar IDL;

service
	: 'service' pkg '{' serviceItem* '}' SEMICOLON
;

pkg
	: (IDENTIFIER '.')* IDENTIFIER
;

serviceItem
	: serviceId
	| interfaceVersion
	| transportProtocol
	| struct
	| interfaceDesc
;

serviceId
	: 'serviceId' INTEGER SEMICOLON
;

interfaceVersion
	: 'interfaceVersion' INTEGER SEMICOLON
;

transportProtocol
	: 'transportProtocol' TRANSPORT_PROTOCOL SEMICOLON
;

struct
	: 'struct' IDENTIFIER '{' structItem* '}' SEMICOLON
;

structItem
	: TYPE IDENTIFIER SEMICOLON
;

interfaceDesc
	: 'interface' IDENTIFIER '{' messageItem* '}' SEMICOLON
;

messageItem
	: 'method' MESSAGETYPE IDENTIFIER '(' parameterItem* ')' SEMICOLON
;

parameterItem
	: TYPE IDENTIFIER
	| COMMA TYPE IDENTIFIER
;

COMMA
	: ','
;

SEMICOLON
	: ';'
;

INTEGER
	: '-'?('0'([Xx][0-9A-Fa-f]+|[0-7]*)|[1-9][0-9]*)
;

FLOAT
	: '-'?(([0-9]+'.'[0-9]*|[0-9]*'.'[0-9]+)([Ee][\+\-]?[0-9]+)?|[0-9]+[Ee][\+\-]?[0-9]+)
;

STRING
	: '"'~['"']*'"'
;

WHITESPACE
	: [\t\n\r ]+ -> channel(HIDDEN)
;

COMMENT
	: ('//'~[\n\r]*|'/*'(.|'\n')*?'*/')+ -> channel(HIDDEN)
; // Note: '/''/'~[\n\r]* instead of '/''/'.* (non-greedy because of wildcard).

OTHER
	: ~[\t\n\r 0-9A-Z_a-z]
;

TRANSPORT_PROTOCOL
    : 'UDP'
    | 'TCP'
;

// See [TR_SOMEIP_00055]
MESSAGETYPE
	: 'REQUEST'
	| 'REQUEST_NO_RETURN'
	| 'NOTIFICATION'
	| 'REQUEST_ACK'
	| 'REQUEST_NO_RETURN_ACK'
	| 'NOTIFICATION_ACK'
	| 'RESPONSE'
	| 'ERROR'
	| 'RESPONSE_ACK'
	| 'ERROR_ACK'
;

// See [TR_SOMEIP_00065]
TYPE
	: 'boolean'
	| 'uint8'
	| 'uint16'
	| 'uint32'
	| 'uint64' // Custom type
	| 'sint8'
	| 'sint16'
	| 'sint32'
	| 'float32'
	| 'float64'
	| 'bytes'  // Custom type
;

IDENTIFIER
	: [A-Z_a-z][0-9A-Z_a-z]*
;
