# SOME/thing #

An open-source SOME/IP stack

## State ##
beta

## Goals ##
- Implement as much of the specification as actually needed
- Possibly port to Misra C later -> Don't make the code too enterprise'y
- Next to no external dependencies on device

## Supported features ##
- Generate proxy/stubs from IDL files
- Support UDP and TCP transport protocols
- Minimal service discovery via multicast sockets
- Hello world client/service with vSomeIP hello world sample
  -> Function call with string parameter but no response
- Periodic promotion of a service
- Instances
- Introspection is supported through Jose Amores Wireshark plugin: https://github.com/jamores/eth-ws-someip

## TODO ##
- Actually consider signedness of datatypes
- Extend HelloWorld example with data-structures and sequences thereof
- Autosar XML codegenerator
- Cleanup handling of endpoint options, endpoint selection etc.
- Use preallocated pools instead of lists
- Consider string (fixed, variable length) and array length requirements ([TR_SOMEIP_00082], [TR_SOMEIP_00091], [TR_SOMEIP_00098], ...)
- Error handling is non-existent 
- Documentation is non-existent
- Add call with response
- Add notifications
- Configuration from file
- Maybe support SSL as a custom transport protocol
- Offer a service as a response to a find service
- Data alignment in serializer/deserializer
- Detection of remote "reboots"